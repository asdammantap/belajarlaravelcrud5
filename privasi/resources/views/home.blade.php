@extends('template/t_index')
@section('content')
<div class="container">
<p></p>
<div class="card bg-default">
<div class="card-header">Tambah Data</div>
<div class="card-body">
{!! Form::open(['url' => '/prosestambah']) !!}
Nama:
{!! Form::text('nama','',['placeholder' => 'Nama','class' => 'form-control']) !!}
Alamat:
{!! Form::text('alamat','',['placeholder' => 'Alamat','class' => 'form-control']) !!}
Kelas:
{!! Form::text('kelas','',['placeholder' => 'Kelas','class' => 'form-control']) !!}
<p></p>
{!! Form::submit('Tambah Data',['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}
@stop
</div>
</div>
</div>