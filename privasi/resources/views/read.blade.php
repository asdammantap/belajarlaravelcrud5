@extends('template/t_index')
@section('content')
<div class="container">
@if(Session::has('message'))
<span class="alert alert-success">{{Session::get('message')}}</span>
@endif
<p></p>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<th>Nama</th>
<th>Alamat</th>
<th>Kelas</th>
</tr>
<?php $no=1;?>
@foreach($siswa as $datasiswa)
<tr>
<td>{{$no++}}</td>
<td>{{$datasiswa->nama}}</td>
<td>{{$datasiswa->alamat}}</td>
<td>{{$datasiswa->kelas}}</td>
<td><a href="hapus/{{$datasiswa->id}}">Hapus</a>||<a href="formedit/{{$datasiswa->id}}">Edit</a></td>
</tr>
@endforeach
</table>
</div>
</div>
@stop