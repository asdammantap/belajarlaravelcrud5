<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class Crudcontroller extends Controller
{
    //
    public function tambahdata(){
        $data=array(
            'nama'=>Input::get('nama'),
            'alamat'=>Input::get('alamat'),
            'kelas'=>Input::get('kelas')
        );
        DB::table('siswa')->insert($data);
        return Redirect::to('/read')->with('message','berhasil tambah data');
    }
    public function lihatdata(){
        $data=DB::table('siswa')->get();
        return View::make('read')->with('siswa',$data);
    }
    public function hapusdata($id){
        DB::table('siswa')->where('id','=',$id)->delete();
        return Redirect::to('/read')->with('message','berhasil hapus data');
    }
    public function editdata($id){
        $data=DB::table('siswa')->where('id','=',$id)->first();
        return View::make('form_edit')->with('siswa',$data);
    }
    public function proseseditdata(){
        $data=array(
            'nama'=>Input::get('nama'),
            'alamat'=>Input::get('alamat'),
            'kelas'=>Input::get('kelas')
        );
        DB::table('siswa')->where('id','=',Input::get('id'))->update($data);
        return Redirect::to('/read')->with('message','berhasil edit data');
    }
}
